#import "SimpleBlePlugin.h"

@import MediaPlayer;

@interface PeripheralContext : NSObject
@property FlutterResult rssiResult;
@end

@implementation PeripheralContext

@end

@interface SimpleBlePlugin()
@property(nonatomic, retain) CBCentralManager *centralManager;
@property(nonatomic, retain) FlutterMethodChannel *channel;
@property(nonatomic, retain) SimpleBleStreamHandler *stateStreamHandler;
@property(nonatomic, retain) SimpleBleStreamHandler *detectedDevicesStreamHandler;
@property(nonatomic, retain) SimpleBleStreamHandler *characteristicNotificationsStreamHandler;
@property(nonatomic, retain) SimpleBleStreamHandler *devicesStateStreamHandler;

@property(nonatomic, retain) SimpleBleStreamHandler *devicesRssiStreamHandler;

@property(nonatomic, retain) NSMutableArray<CBPeripheral*> *knownPeripherals;

@property FlutterResult methodResult;
@property FlutterResult servicesResult;
@property FlutterResult characteristicsResult;

@property NSObject<FlutterPluginRegistrar> *registrar;
@property FlutterEngine *headlessRunner;
@property NSUserDefaults *persistentState;
@property FlutterMethodChannel *callbackChannel;

@property(nonatomic, retain) NSMutableDictionary<NSString*, PeripheralContext*> *peripheralContexts;

@end


@implementation SimpleBleStreamHandler

- (FlutterError*)onListenWithArguments:(id)arguments eventSink:(FlutterEventSink)eventSink {
    
    self.sink = eventSink;
    
    return nil;
}

- (FlutterError*)onCancelWithArguments:(id)arguments {
    self.sink = nil;
    return nil;
}

@end

@implementation SimpleBlePlugin

static FlutterPluginRegistrantCallback registerPlugins = nil;
static BOOL backgroundIsolateRun = NO;
static BOOL initialized = NO;

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    
    FlutterMethodChannel* channel = [FlutterMethodChannel
                                     methodChannelWithName:@"simple_ble"
                                     binaryMessenger:[registrar messenger]];
    
    
    SimpleBlePlugin* instance = [[SimpleBlePlugin alloc] init];
    [registrar addMethodCallDelegate:instance channel:channel];
    [registrar addApplicationDelegate:instance];
    
    instance.registrar=registrar;
    
    instance.peripheralContexts = [NSMutableDictionary dictionary];
    
    SimpleBleStreamHandler* stateStreamHandler = [[SimpleBleStreamHandler alloc] init];
    FlutterEventChannel* stateEventChannel = [FlutterEventChannel eventChannelWithName: @"state" binaryMessenger:[registrar messenger]];
    [stateEventChannel setStreamHandler:stateStreamHandler];
    instance.stateStreamHandler = stateStreamHandler;
    
    SimpleBleStreamHandler* detectedDevicesStreamHandler = [[SimpleBleStreamHandler alloc] init];
    FlutterEventChannel* detectedDevicesEventChannel = [FlutterEventChannel eventChannelWithName: @"detected-devices" binaryMessenger:[registrar messenger]];
    [detectedDevicesEventChannel setStreamHandler:detectedDevicesStreamHandler];
    instance.detectedDevicesStreamHandler = detectedDevicesStreamHandler;
    
    SimpleBleStreamHandler* characteristicNotificationsStreamHandler = [[SimpleBleStreamHandler alloc] init];
    FlutterEventChannel* characteristicNotificationsEventChannel = [FlutterEventChannel eventChannelWithName: @"notifications" binaryMessenger:[registrar messenger]];
    [characteristicNotificationsEventChannel setStreamHandler:characteristicNotificationsStreamHandler];
    instance.characteristicNotificationsStreamHandler = characteristicNotificationsStreamHandler;
    
    
    SimpleBleStreamHandler* devicesStateStreamHandler = [[SimpleBleStreamHandler alloc] init];
    FlutterEventChannel* devicesStateEventChannel = [FlutterEventChannel eventChannelWithName: @"devices-state" binaryMessenger:[registrar messenger]];
    [devicesStateEventChannel setStreamHandler:devicesStateStreamHandler];
    instance.devicesStateStreamHandler = devicesStateStreamHandler;
    
    
    SimpleBleStreamHandler* devicesRssiStreamHandler = [[SimpleBleStreamHandler alloc] init];
    FlutterEventChannel* rssiEventChannel = [FlutterEventChannel eventChannelWithName: @"devices-rssi" binaryMessenger:[registrar messenger]];
    [rssiEventChannel setStreamHandler:devicesRssiStreamHandler];
    instance.devicesRssiStreamHandler = devicesRssiStreamHandler;
    
    
    NSDictionary *options = @{CBCentralManagerOptionRestoreIdentifierKey: @"simple-ble"};
    instance.centralManager = [[CBCentralManager alloc] initWithDelegate:instance queue:nil options: options];
    
    instance.knownPeripherals = [NSMutableArray array];
    
    instance.persistentState = [NSUserDefaults standardUserDefaults];
    instance.headlessRunner = [[FlutterEngine alloc] initWithName:@"SimpleBleIsolate" project:nil allowHeadlessExecution:YES];
    instance.callbackChannel =
    [FlutterMethodChannel methodChannelWithName:@"plugins.flutter.io/simpleble_plugin_background"
                                binaryMessenger:instance.headlessRunner];
}

+ (void)setPluginRegistrantCallback:(FlutterPluginRegistrantCallback)callback {
    registerPlugins = callback;
}

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions {
    NSArray *centralManagerIdentifiers =
    launchOptions[UIApplicationLaunchOptionsBluetoothCentralsKey];
    if ([centralManagerIdentifiers count] > 0) {
        for (NSString *identifier in centralManagerIdentifiers) {
            self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{CBPeripheralManagerOptionRestoreIdentifierKey : identifier}];
        }
        [self startBleService:[self getCallbackDispatcherHandle]];
        int64_t handle=[self getCallbackForRestoreStateHandle];
        [self.callbackChannel invokeMethod:@"app_restarted_bluetooth" arguments: @[@(handle)]];
        
    }
    return true;
}

-(void) localNotifWithTitle: (NSString*)title andBody: (NSString*)body {
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    localNotif.alertBody= body;
    [[UIApplication sharedApplication] presentLocalNotificationNow: localNotif];
}

- (void)startBleService:(int64_t)handle {
    [self setCallbackDispatcherHandle:handle];
    FlutterCallbackInformation *info = [FlutterCallbackCache lookupCallbackInformation:handle];
    NSAssert(info != nil, @"failed to find callback");
    NSString *entrypoint = info.callbackName;
    NSString *uri = info.callbackLibraryPath;
    [_headlessRunner runWithEntrypoint:entrypoint libraryURI:uri];
    NSAssert(registerPlugins != nil, @"failed to set registerPlugins");
    if (!backgroundIsolateRun) {
        registerPlugins(_headlessRunner);
    }
    [_registrar addMethodCallDelegate:self channel:_callbackChannel];
    backgroundIsolateRun = YES;
}

- (void)setCallbackDispatcherHandle:(int64_t)handle {
    [self.persistentState setObject:[NSNumber numberWithLongLong:handle]
                             forKey:@"callback_dispatcher_handle"];
}

- (void)setCallbackForRestoreStateHandle:(int64_t)handle {
    [self.persistentState setObject:[NSNumber numberWithLongLong:handle]
                             forKey:@"callback_restorestate_handle"];
}

- (int64_t)getCallbackDispatcherHandle {
    id handle = [_persistentState objectForKey:@"callback_dispatcher_handle"];
    if (handle == nil) {
        return 0;
    }
    return [handle longLongValue];
}

- (int64_t)getCallbackForRestoreStateHandle {
    id handle = [_persistentState objectForKey:@"callback_restorestate_handle"];
    if (handle == nil) {
        return 0;
    }
    return [handle longLongValue];
}



- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    NSArray *arguments = call.arguments;
    if ([@"initializeService" isEqualToString:call.method]) {
        [self startBleService:[arguments[0] longValue]];
        result(@(YES));
    } else if ([@"didRestoreState" isEqualToString:call.method]) {
        int64_t callbackHandle = [call.arguments longLongValue];
        [self setCallbackForRestoreStateHandle:callbackHandle];
    } else if ([@"initialized" isEqualToString:call.method]) {
        @synchronized(self) {
            initialized = YES;
        }
        result(nil);
    } else if ([@"isAvailable" isEqualToString:call.method]) {
        if (@available(iOS 10.0, *)) {
            if(self.centralManager.state != CBManagerStateUnsupported && self.centralManager.state != CBManagerStateUnknown) {
                result(@(YES));
            } else {
                result(@(NO));
            }
        }  else {
            // Fallback on earlier versions
        }
    } else if ([@"bleState" isEqualToString:call.method]) {
        result(@(self.centralManager.state));
    } else if ([@"startScan" isEqualToString:call.method]) {
        NSArray<NSString*> *services = call.arguments[@"serviceUUIDs"];
        NSDictionary *options = call.arguments[@"options"];
        
        NSMutableArray<CBUUID*> *servicesUUIDs = nil;
        NSMutableDictionary *scanOptions = nil;
        
        if (services && services != (id)[NSNull null] && services.count > 0) {
            servicesUUIDs=[NSMutableArray array];
            for (NSString *uuidStr in services) {
                [servicesUUIDs addObject: [CBUUID UUIDWithString: uuidStr]];
            }
        }
        
        if (options && options != (id)[NSNull null] && options.count > 0) {
            scanOptions = [NSMutableDictionary dictionary];
            if (options[@"ios-CBCentralManagerScanOptionAllowDuplicatesKey"]) {
                [scanOptions setObject: @(true) forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
            }
        }
        
        [self.centralManager scanForPeripheralsWithServices: servicesUUIDs options: scanOptions];
        result(nil);
    } else if ([@"stopScan" isEqualToString:call.method]) {
        [self.centralManager stopScan];
        result(nil);
    } else if ([@"devicesWithIdentifiers" isEqualToString:call.method]) {
        NSArray<NSString*> *identifiers=call.arguments[@"identifiers"];
        NSLog(@"devicesWithIdentifiers to %@", identifiers);
        NSMutableArray<NSUUID*> *deviceUUIDs = [NSMutableArray array];
        for (NSString *identifierStr in identifiers) {
            NSUUID *uuid= [[NSUUID alloc] initWithUUIDString:identifierStr];
            [deviceUUIDs addObject: uuid];
        }
        NSArray<CBPeripheral*> *peripherals=[self.centralManager retrievePeripheralsWithIdentifiers: deviceUUIDs];
        NSMutableArray *devices = [NSMutableArray array];
        for (CBPeripheral *peripheral in peripherals) {
            [devices addObject:
             @{
                 @"identifier": [peripheral.identifier UUIDString],
                 @"name": peripheral.name == nil ? [NSNull null]: peripheral.name,
                 @"state": @(peripheral.state),
             }];
        }
        result(devices);
        
    } else if ([@"connectedDevices" isEqualToString:call.method]) {
        NSArray *services = call.arguments[@"serviceUUIDs"];
        NSLog(@"connectedDevices with services: %@", services);
        
        NSMutableArray<CBUUID*> *serviceUUIDs = nil;
        if (services && services != (id)[NSNull null] && services.count > 0) {
            serviceUUIDs=[NSMutableArray array];
            for (NSString *uuidStr in services) {
                [serviceUUIDs addObject: [CBUUID UUIDWithString: uuidStr]];
            }
        }
        NSLog(@"Filtered services: %@", serviceUUIDs);
        
        NSArray<CBPeripheral*> *peripherals=[self.centralManager retrieveConnectedPeripheralsWithServices: serviceUUIDs];
        NSMutableArray *devices = [NSMutableArray array];
        for (CBPeripheral *peripheral in peripherals) {
            [devices addObject:
             @{
                 @"identifier": [peripheral.identifier UUIDString],
                 @"name": peripheral.name == nil ? [NSNull null]: peripheral.name,
                 @"state": @(peripheral.state),
             }];
        }
        result(devices);
        
    } else if ([@"connectDevice" isEqualToString:call.method]) {
        NSString *identifier=call.arguments;
        NSLog(@"connectDevice to %@", identifier);
        NSUUID *uuid= [[NSUUID alloc] initWithUUIDString:identifier];
        CBPeripheral *peripheral = [self peripheralWithIdentifier:uuid];
        if (peripheral) {
            [_knownPeripherals addObject: peripheral];
            peripheral.delegate=self;
            [self.centralManager connectPeripheral: peripheral options: nil];
        }
        result(nil);
    } else if ([@"cancelDeviceConnection" isEqualToString:call.method]) {
        NSString *identifier=call.arguments;
        NSLog(@"cancelDeviceConnection to %@", identifier);
        NSUUID *uuid= [[NSUUID alloc] initWithUUIDString:identifier];
        CBPeripheral *peripheral = [self peripheralWithIdentifier:uuid];
        if (peripheral) {
            [self.centralManager cancelPeripheralConnection: peripheral];
        }
        result(nil);
    } else if ([@"discoverServices" isEqualToString:call.method]) {
        NSString *identifier=call.arguments[@"identifier"];
        NSArray *services = call.arguments[@"serviceUUIDs"];
        NSLog(@"discoverServices to %@", identifier);
        
        NSMutableArray<CBUUID*> *servicesUUIDs = nil;
        if (services && services != (id)[NSNull null] && services.count > 0) {
            servicesUUIDs=[NSMutableArray array];
            for (NSString *uuidStr in services) {
                [servicesUUIDs addObject: [CBUUID UUIDWithString: uuidStr]];
            }
        }
        NSLog(@"Filtered services: %@", servicesUUIDs);
        
        NSUUID *uuid= [[NSUUID alloc] initWithUUIDString:identifier];
        CBPeripheral *peripheral = [self peripheralWithIdentifier:uuid];
        if (peripheral) {
            self.servicesResult=result;
            [peripheral discoverServices: servicesUUIDs];
        }
    } else if ([@"services" isEqualToString:call.method]) {
        NSString *identifier=call.arguments[@"identifier"];
        NSLog(@"services to %@", identifier);
        
        NSUUID *uuid= [[NSUUID alloc] initWithUUIDString:identifier];
        CBPeripheral *peripheral = [self peripheralWithIdentifier:uuid];
        if (peripheral) {
            NSMutableArray *serviceUUIDs = [NSMutableArray array];
            for (CBService *service in peripheral.services) {
                [serviceUUIDs addObject: @{@"uuid": [service.UUID UUIDString]}];
            }
            result(serviceUUIDs);
        }
    } else if ([@"discoverCharacteristics" isEqualToString:call.method]) {
        NSString *identifier=call.arguments[@"identifier"];
        NSArray *characteristics = call.arguments[@"characteristicUUIDs"];
        NSDictionary *service = call.arguments[@"service"];
        
        NSLog(@"discoverCharacteristics to %@", identifier);
        
        NSMutableArray<CBUUID*> *characteristicUUIDs = nil;
        if (characteristics && characteristics != (id)[NSNull null] && characteristics.count > 0) {
            characteristicUUIDs=[NSMutableArray array];
            for (NSString *uuidStr in characteristics) {
                [characteristicUUIDs addObject: [CBUUID UUIDWithString: uuidStr]];
            }
        }
        
        NSUUID *uuid= [[NSUUID alloc] initWithUUIDString:identifier];
        CBPeripheral *peripheral = [self peripheralWithIdentifier:uuid];
        if (peripheral) {
            NSArray<CBService*> *services= peripheral.services;
            for (CBService *serviceIt in services) {
                if ([[serviceIt.UUID UUIDString] isEqualToString: service[@"uuid"]]) {
                    self.characteristicsResult=result;
                    [peripheral discoverCharacteristics: characteristicUUIDs forService: serviceIt];
                    break;
                }
            }
            
        }
    } else if ([@"writeValueForCharacteristic" isEqualToString:call.method]) {
        NSString *identifier=call.arguments[@"identifier"];
        NSDictionary *characteristic = call.arguments[@"characteristic"];
        char value = [call.arguments[@"value"] charValue];
        int type = [call.arguments[@"type"] intValue];
        
        NSString *characteristicUuid = characteristic[@"uuid"];
        NSString *serviceUuid = characteristic[@"serviceUuid"];
        
        NSLog(@"writeValueForCharacteristic for deviceUUID: %@ Characteristic: %@ ServiceUUID: %@ Type: %d", identifier, characteristicUuid, serviceUuid, type);
        
        NSUUID *uuid= [[NSUUID alloc] initWithUUIDString:identifier];
        CBPeripheral *peripheral = [self peripheralWithIdentifier:uuid];
        if (peripheral) {
            CBService *service = [self serviceWithUuid:serviceUuid fromPeripheral: peripheral];
            if (service) {
                CBCharacteristic *characteristic = [self characteristicWithUuid:characteristicUuid fromService:service];
                if (characteristic) {
                    NSData *data=[NSData dataWithBytes: &value length: sizeof(value)];
                    [peripheral writeValue: data  forCharacteristic: characteristic type: CBCharacteristicWriteWithoutResponse];
                }
            }
        }
        result(nil);
    } else if ([@"readValueForCharacteristic" isEqualToString:call.method]) {
        
        NSString *identifier=call.arguments[@"identifier"];
        NSDictionary *characteristic = call.arguments[@"characteristic"];
        
        NSString *characteristicUuid = characteristic[@"uuid"];
        NSString *serviceUuid = characteristic[@"serviceUuid"];
        
        NSLog(@"readValueForCharacteristic to %@ %@ %@", identifier, characteristicUuid, serviceUuid);
        
        NSUUID *uuid= [[NSUUID alloc] initWithUUIDString:identifier];
        CBPeripheral *peripheral = [self peripheralWithIdentifier:uuid];
        if (peripheral) {
            CBService *service = [self serviceWithUuid:serviceUuid fromPeripheral: peripheral];
            if (service) {
                CBCharacteristic *characteristic = [self characteristicWithUuid:characteristicUuid fromService:service];
                if (characteristic) {
                    self.methodResult=result;
                    [peripheral readValueForCharacteristic: characteristic];
                }
            }
        }
    }  else if ([@"setNotifyValueForCharacteristic" isEqualToString:call.method]) {
        
        NSString *identifier=call.arguments[@"identifier"];
        NSDictionary *characteristic = call.arguments[@"characteristic"];
        
        NSString *characteristicUuid = characteristic[@"uuid"];
        NSString *serviceUuid = characteristic[@"serviceUuid"];
        BOOL enabled = call.arguments[@"enabled"];
        
        NSLog(@"setNotifyValueForCharacteristic to %@ %@ %@ %@", identifier, characteristicUuid, serviceUuid, enabled?@"TRUE": @"FALSE");
        
        NSUUID *uuid= [[NSUUID alloc] initWithUUIDString:identifier];
        CBPeripheral *peripheral = [self peripheralWithIdentifier:uuid];
        if (peripheral) {
            CBService *service = [self serviceWithUuid:serviceUuid fromPeripheral: peripheral];
            if (service) {
                CBCharacteristic *characteristic = [self characteristicWithUuid:characteristicUuid fromService:service];
                if (characteristic) {
                    [peripheral setNotifyValue: enabled forCharacteristic:characteristic];
                }
            }
        }
        result(nil);
    } else if ([@"isScaning" isEqualToString:call.method]) {
        if (@available(iOS 9.0, *)) {
            return result(@(self.centralManager.isScanning));
        } else {
            // Fallback on earlier versions
        }
    } else if ([@"readRssi" isEqualToString:call.method]) {
        NSString *identifier=call.arguments;
        NSUUID *uuid= [[NSUUID alloc] initWithUUIDString:identifier];
        CBPeripheral *peripheral = [self peripheralWithIdentifier:uuid];
        if (peripheral) {
            PeripheralContext *context=[self.peripheralContexts objectForKey: peripheral.identifier.UUIDString];
            if (context == nil) {
                context = [[PeripheralContext alloc] init];
                [self.peripheralContexts setObject:context forKey:peripheral.identifier.UUIDString];
            }
            context.rssiResult = result;
            [peripheral readRSSI];
        } else {
            result([FlutterError errorWithCode:@"UNAVAILABLE"
                                       message:@"Peripheral not found"
                                       details:nil]);
        }
    } else {
        result(FlutterMethodNotImplemented);
    }
}




-(void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary<NSString *,id> *)dict {
    NSLog(@"willRestoreState");
    NSArray *peripherals = dict[CBCentralManagerRestoredStatePeripheralsKey];
    for (CBPeripheral *peripheral in peripherals) {
        NSLog(@"RestoringStatefor peripheral: %@", peripheral);
        peripheral.delegate=self;
        [_knownPeripherals addObject: peripheral];
    }
}

-(CBService*)serviceWithUuid: (NSString*)uuid fromPeripheral: (CBPeripheral*)peripheral {
    for (CBService *service in peripheral.services) {
        if ([[service.UUID UUIDString] isEqualToString: uuid]) {
            return service;
        }
    }
    return nil;
}

-(CBCharacteristic*)characteristicWithUuid: (NSString*)uuid fromService: (CBService*)service {
    for (CBCharacteristic *characteristic in service.characteristics) {
        if ([[characteristic.UUID UUIDString] isEqualToString: uuid]) {
            return characteristic;
        }
    }
    return nil;
}

-(CBPeripheral*)peripheralWithIdentifier: (NSUUID *)identifier {
    NSArray<CBPeripheral*> *peripherals = [self.centralManager retrievePeripheralsWithIdentifiers:@[identifier]];
    if (peripherals && peripherals.count == 1) {
        CBPeripheral *peripheral=[peripherals firstObject];
        return peripheral;
    }
    return nil;
}

//
// CBCentralManagerDelegate methods
//

- (void)centralManagerDidUpdateState:(nonnull CBCentralManager *)central {
    NSLog(@"centralManagerDidUpdateState: %@", @(central.state));
    if(_stateStreamHandler.sink != nil) {
        self.stateStreamHandler.sink(@(central.state));
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI {
    if(_detectedDevicesStreamHandler.sink != nil) {
        
        NSDictionary *dicoService = [advertisementData objectForKey: @"kCBAdvDataServiceData"];
        NSMutableDictionary *serviceData = [NSMutableDictionary dictionary];
        
        for (CBUUID *key in [dicoService allKeys]) {
            NSData *data =[dicoService objectForKey: key];
            [serviceData setObject: data forKey: [key UUIDString]];
        }
        
        self.detectedDevicesStreamHandler.sink(@{
            @"rssi": RSSI,
            @"device": @{
                    @"identifier": [peripheral.identifier UUIDString],
                    @"name": peripheral.name == nil ? [NSNull null]: peripheral.name,
                    @"state": @(peripheral.state),
            },
            @"serviceData": serviceData,
        });
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"didConnectPeripheral: %@", peripheral.identifier);
    if (_devicesStateStreamHandler.sink != nil) {
        self.devicesStateStreamHandler.sink(@{
        @"state": @(peripheral.state),
        @"identifier": [peripheral.identifier UUIDString]});
    }
    peripheral.delegate=self;
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"didDisconnectPeripheral: %@ with error: %d", peripheral.identifier, error.code);
    if (_devicesStateStreamHandler.sink != nil) {
        int cause = 0;
        if (error != nil) {
            cause = error.code;
        }
        self.devicesStateStreamHandler.sink(@{
        @"state": @(peripheral.state),
        @"identifier": [peripheral.identifier UUIDString],
        @"cause": @(cause)});
    }
    peripheral.delegate=nil;
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"didFailToConnectPeripheral");
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"didDiscoverServices");
    if (!error) {
        NSLog(@"Services: %@", peripheral.services);
        if (self.servicesResult) {
            NSMutableArray *serviceUUIDs = [NSMutableArray array];
            for (CBService *service in peripheral.services) {
                [serviceUUIDs addObject: @{@"uuid": [service.UUID UUIDString]}];
            }
            self.servicesResult(serviceUUIDs);
        }
    } else {
        
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error {
    if(_devicesRssiStreamHandler.sink != nil) {
        _devicesRssiStreamHandler.sink(@{
        @"rssi": RSSI,
        @"identifier": [peripheral.identifier UUIDString],
        });
    }
    PeripheralContext *context=[self.peripheralContexts objectForKey: peripheral.identifier.UUIDString];
    if (context) {
        if (error) {
            context.rssiResult([FlutterError errorWithCode:@"Error reading RSSI"
                                                message:error.localizedDescription
                                                details:nil]);
        } else {
            context.rssiResult(RSSI);
        }
    } else {
        NSLog(@"Can not get context for peripheral: %@", peripheral.identifier.UUIDString);
    }
    
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"didDiscoverCharacteristicsForService");
    if (!error) {
        if (self.characteristicsResult) {
            NSMutableArray *characteristicUUIDs = [NSMutableArray array];
            for (CBCharacteristic *characteristic in service.characteristics) {
                [characteristicUUIDs addObject: @{@"service": @{@"uuid": [service.UUID UUIDString]}, @"characteristic": @{@"uuid":[characteristic.UUID UUIDString]}}];
            }
            NSLog(@"%@", characteristicUUIDs);
            self.characteristicsResult(characteristicUUIDs);
        }
    } else {
        ;
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"didWriteValueForCharacteristic");
    if (!error) {
        ;
    } else {
        NSLog(@"Error: %@", error.localizedDescription);
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"didUpdateValueForCharacteristic");

    if (!error) {
        NSData *data=characteristic.value;
        char value;
        [data getBytes:&value length: 1];
        
        if (self.methodResult) {
            self.methodResult(@{@"value": data, @"characteristic": @{@"uuid":[characteristic.UUID UUIDString]}});
        }
        if (self.characteristicNotificationsStreamHandler) {
            self.characteristicNotificationsStreamHandler.sink(@{@"value": data, @"characteristic": @{@"uuid":[characteristic.UUID UUIDString]}});
        }
        
    } else {
        NSLog(@"Error: %@", error.localizedDescription);
    }
}

-(void) updateVolume {
    MPVolumeView* volumeView = [[MPVolumeView alloc] init];
    
    // Get the Volume Slider
    UISlider* volumeViewSlider = nil;
    
    for (UIView *view in [volumeView subviews]){
        if ([view.class.description isEqualToString:@"MPVolumeSlider"]){
            volumeViewSlider = (UISlider*)view;
            break;
        }
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [volumeViewSlider setValue:1.0f animated:YES];
        [volumeViewSlider sendActionsForControlEvents:UIControlEventTouchUpInside];
    });
}

@end

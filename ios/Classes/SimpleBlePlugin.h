#import <Flutter/Flutter.h>
#import <CoreBluetooth/CoreBluetooth.h>


@interface SimpleBlePlugin : NSObject<FlutterPlugin, CBCentralManagerDelegate, CBPeripheralDelegate>
@end


@interface SimpleBleStreamHandler : NSObject<FlutterStreamHandler>
@property FlutterEventSink sink;
@end




import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:simple_ble/callback_dispatcher.dart';

class SimpleBle {
  static const MethodChannel _methodChannel = const MethodChannel('simple_ble');
  static const EventChannel _stateEventChannel = const EventChannel('state');
  static const EventChannel _detectedDevicesEventChannel =
      const EventChannel('detected-devices');

  static const EventChannel _devicesStateEventChannel =
      const EventChannel('devices-state');

  static const EventChannel _devicesRssiEventChannel =
      const EventChannel('devices-rssi');

  static const EventChannel _characteristicNotificationsEventChannel =
      const EventChannel('notifications');

  static Stream<BleState> _bleStateStream;
  static Stream<BleDeviceStateChanged> _deviceStatesStream;

  SimpleBle() {}

  static Future<void> initialize() async {
    final CallbackHandle callback =
        PluginUtilities.getCallbackHandle(callbackDispatcher);
    await _methodChannel
        .invokeMethod('initializeService', <dynamic>[callback.toRawHandle()]);
  }

  static Future<bool> didRestoreState(void Function() callback) {
    if (callback == null) {
      throw ArgumentError.notNull('callback');
    }
    final CallbackHandle handle = PluginUtilities.getCallbackHandle(callback);
    return _methodChannel
        .invokeMethod("didRestoreState", handle.toRawHandle())
        .then<bool>((dynamic result) {
      return result;
    });
  }

  static Future<String> get platformVersion async {
    final String version =
        await _methodChannel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<bool> get isAvailable async {
    final bool isAvailable = await _methodChannel.invokeMethod('isAvailable');
    return isAvailable;
  }

  static Future<BleState> get state async {
    final res = await _methodChannel.invokeMethod('bleState');
    return bleStateFromValue(res);
  }

  static Future<bool> get isScaning async {
    final bool isScaning = await _methodChannel.invokeMethod('isScaning');
    return isScaning;
  }

  static Future<void> enableBle() async {
    await _methodChannel.invokeMethod('enableBle');
  }

  static Future<void> disableBle() async {
    await _methodChannel.invokeMethod('disableBle');
  }

  static Stream<ScanResult> startScan(
      {List<String> serviceUUIDs, Map<dynamic, dynamic> options}) async* {
    await _methodChannel.invokeMethod(
        'startScan', {"serviceUUIDs": serviceUUIDs, "options": options});
    yield* _detectedDevicesEventChannel.receiveBroadcastStream().map((event) {
      return ScanResult.fromMap(event);
    });
  }

  static Future<void> stopScan() async {
    await _methodChannel.invokeMethod('stopScan');
  }

  static Future<List<BleDevice>> devicesWithIdentifiers(
      List<String> identifiers) async {
    final res = await _methodChannel
        .invokeMethod('devicesWithIdentifiers', {"identifiers": identifiers});
    final List<BleDevice> devices =
        res.map<BleDevice>((event) => BleDevice.fromMap(event)).toList();
    return devices;
  }

  static Future<List<BleDevice>> connectedDevices(
      List<String> serviceUuids) async {
    final res = await _methodChannel
        .invokeMethod('connectedDevices', {'serviceUUIDs': serviceUuids});
    final List<BleDevice> devices =
        res.map<BleDevice>((event) => BleDevice.fromMap(event)).toList();
    return devices;
  }

  static Future<void> connectDevice(String deviceId) async {
    await _methodChannel.invokeMethod('connectDevice', deviceId);
  }

  static Future<void> cancelDeviceConnection(String deviceId) async {
    await _methodChannel.invokeMethod('cancelDeviceConnection', deviceId);
  }

  static Stream<BleState> get onStateChanged {
    if (_bleStateStream == null) {
      _bleStateStream = _stateEventChannel
          .receiveBroadcastStream()
          .map((state) => bleStateFromValue(state));
    }
    return _bleStateStream;
  }

  static BleDeviceState _bleDeviceStateFromValue(int val) {
    switch (val) {
      case 0:
        return BleDeviceState.disconnected;
      case 1:
        return BleDeviceState.connecting;
      case 2:
        return BleDeviceState.connected;
      case 3:
        return BleDeviceState.disconnecting;
      default:
        return BleDeviceState.unknown;
    }
  }

  static Stream<BleDeviceStateChanged> get onDeviceStateChanged {
    if (_deviceStatesStream == null) {
      _deviceStatesStream =
          _devicesStateEventChannel.receiveBroadcastStream().map((event) {
        final state = _bleDeviceStateFromValue(event['state']);
        final cause = event['cause'];
        final identifier = event['identifier'];
        return BleDeviceStateChanged(
            identifier: identifier, state: state, cause: cause);
      });
    }
    return _deviceStatesStream;
  }

  static Stream<Rssi> get onDeviceRssiChanged {
    return _devicesRssiEventChannel.receiveBroadcastStream().map((event) {
      final value = event['rssi'];
      final identifier = event['identifier'];
      return Rssi(identifier: identifier, value: value);
    });
  }

  static BleState bleStateFromValue(int value) {
    switch (value) {
      case 0:
        return BleState.unknown;
      case 1:
        return BleState.resetting;
      case 2:
        return BleState.unsupported;
      case 3:
        return BleState.unauthorized;
      case 4:
        return BleState.off;
      case 5:
        return BleState.on;
      case 6:
        return BleState.turningOff;
      case 7:
        return BleState.turningOn;
      default:
        return BleState.unknown;
    }
  }
}

class ScanResult {
  final int rssi;
  final BleDevice device;
  final Map<String, List<int>> serviceData;

  factory ScanResult.fromMap(Map<dynamic, dynamic> map) {
    Map<String, List<int>> serviceData =
        map['serviceData']?.cast<String, List<int>>();
    return ScanResult(
        BleDevice.fromMap(map['device']), map['rssi'], serviceData ?? null);
  }

  ScanResult(this.device, this.rssi, this.serviceData);

  @override
  String toString() {
    return 'ScanResult{rssi: $rssi, device: $device, serviceData: $serviceData}';
  }
}

class BleService {
  String uuid;

  BleService(this.uuid);

  factory BleService.fromMap(Map<dynamic, dynamic> map) {
    return BleService(map['uuid']);
  }

  Map<String, dynamic> toMap() {
    return {
      'uuid': uuid,
    };
  }

  @override
  String toString() {
    return 'BleService{uuid: $uuid}';
  }
}

class BleCharacteristic {
  String uuid;
  Uint8List value;
  String serviceUuid;

  BleCharacteristic(this.uuid, this.serviceUuid, this.value);

  factory BleCharacteristic.fromMap(Map<dynamic, dynamic> map) {
    return BleCharacteristic(
        map['characteristic']['uuid'], map['service']['uuid'], map['value']);
  }

  Map<String, dynamic> toMap() {
    return {
      'uuid': uuid,
      'serviceUuid': serviceUuid,
      'value': value,
    };
  }

  @override
  String toString() {
    return 'BleCharacteristic{uuid: $uuid, value: $value, serviceUuid: $serviceUuid}';
  }
}

class BleDevice {
  final String identifier;
  final String name;
  final BleDeviceState state;
  Timer _rssiTimer;
  Stream<BleDeviceStateChanged> _deviceStateEvents;

  BleDevice(this.identifier, this.name, this.state);

  factory BleDevice.fromMap(Map<dynamic, dynamic> map) {
    return BleDevice(
        map['identifier'], map['name'], bleDeviceStateFromValue(map['state']));
  }

  Map<String, dynamic> toMap() {
    return {
      'identifier': identifier,
      'name': name,
      'state': valueFromBleDeviceState(state)
    };
  }

  @override
  String toString() {
    return 'BleDevice{identifier: $identifier, name: $name, state: $state}';
  }

  static BleDeviceState bleDeviceStateFromValue(int val) {
    switch (val) {
      case 0:
        return BleDeviceState.disconnected;
      case 1:
        return BleDeviceState.connecting;
      case 2:
        return BleDeviceState.connected;
      case 3:
        return BleDeviceState.disconnecting;
      default:
        return BleDeviceState.unknown;
    }
  }

  static valueFromBleDeviceState(BleDeviceState state) {
    switch (state) {
      case BleDeviceState.disconnected:
        return 0;
      case BleDeviceState.connecting:
        return 1;
      case BleDeviceState.connected:
        return 2;
      case BleDeviceState.disconnecting:
        return 3;
      case BleDeviceState.unknown:
        return -1;
    }
  }

  static BleWriteCharacteristicType bleCharacteristicTypeFromValue(int val) {
    switch (val) {
      case 0:
        return BleWriteCharacteristicType.writeWithResponse;
      case 1:
        return BleWriteCharacteristicType.writeWithoutResponse;
    }
  }

  static valueFromBleCharacteristicType(BleWriteCharacteristicType type) {
    switch (type) {
      case BleWriteCharacteristicType.writeWithResponse:
        return 0;
      case BleWriteCharacteristicType.writeWithoutResponse:
        return 1;
    }
  }

  Future<List<BleService>> discoverServices({List<String> serviceUUIDs}) async {
    final res = await SimpleBle._methodChannel.invokeMethod('discoverServices',
        {'identifier': identifier, 'serviceUUIDs': serviceUUIDs});

    List<BleService> services = [];
    res.forEach((event) {
      final service = BleService.fromMap(event);
      services.add(service);
    });
    return services;
  }

  Future<List<BleService>> get services async {
    final res = await SimpleBle._methodChannel
        .invokeMethod('services', {'identifier': identifier});

    List<BleService> services = [];
    res.forEach((event) {
      final service = BleService.fromMap(event);
      services.add(service);
    });
    return services;
  }

  Future<List<BleCharacteristic>> discoverCharacteristics(BleService service,
      {List<String> characteristicUUIDs}) async {
    final res =
        await SimpleBle._methodChannel.invokeMethod('discoverCharacteristics', {
      'identifier': identifier,
      'characteristicUUIDs': characteristicUUIDs,
      'service': service.toMap()
    });

    List<BleCharacteristic> characteristics = [];
    res.forEach((event) {
      final characteristic = BleCharacteristic.fromMap(event);
      characteristics.add(characteristic);
    });
    return characteristics;
  }

  writeValueForCharacteristic(BleCharacteristic characteristic, int value,
      BleWriteCharacteristicType type) async {
    await SimpleBle._methodChannel.invokeMethod('writeValueForCharacteristic', {
      'identifier': identifier,
      'characteristic': characteristic.toMap(),
      'value': value,
      'type': valueFromBleCharacteristicType(type)
    });
  }

  Future<Uint8List> readValueForCharacteristic(
      BleCharacteristic characteristic) async {
    final res = await SimpleBle._methodChannel
        .invokeMethod('readValueForCharacteristic', {
      'identifier': identifier,
      'characteristic': characteristic.toMap(),
    });
    return res["value"];
  }

  Stream<BleCharacteristic> setNotifyValueForCharacteristic(
      BleCharacteristic characteristic, bool enabled) async* {
    await SimpleBle._methodChannel
        .invokeMethod('setNotifyValueForCharacteristic', {
      'identifier': identifier,
      'characteristic': characteristic.toMap(),
      'enabled': enabled,
    });

    yield* SimpleBle._characteristicNotificationsEventChannel
        .receiveBroadcastStream()
        .map((events) {
      BleCharacteristic characteristic = BleCharacteristic(
          events['characteristic']['uuid'],
          events['characteristic']['uuid'],
          events['value']);
      return characteristic;
    });
  }

  Stream<int> readRssi(
      {Duration duration = const Duration(milliseconds: 1000)}) async* {
    while (true) {
      await Future.delayed(duration);
      final rssi =
          await SimpleBle._methodChannel.invokeMethod('readRssi', identifier);
      yield rssi;
    }
  }

  Stream<Rssi> get onRssiChanged {
    return SimpleBle.onDeviceRssiChanged
        .where((rssiChanged) => rssiChanged.identifier == identifier);
  }

  Stream<BleDeviceStateChanged> get onStateChanged {
    return SimpleBle.onDeviceStateChanged.where(
        (deviceStateChanged) => deviceStateChanged.identifier == identifier);
  }
}

class Rssi {
  final String identifier;
  final int value;

  Rssi({@required this.identifier, @required this.value});

  @override
  String toString() {
    return 'Rssi{identifier: $identifier, value: $value}';
  }
}

class BleDeviceStateChanged {
  final String identifier;
  final BleDeviceState state;
  final int cause;

  BleDeviceStateChanged(
      {@required this.identifier, @required this.state, this.cause});

  @override
  String toString() {
    return 'BleDeviceStateChanged{identifier: $identifier, state: $state, cause: $cause}';
  }
}

enum BleDeviceState {
  connected,
  connecting,
  disconnected,
  disconnecting,
  unknown
}

enum BleState {
  off,
  on,
  turningOff,
  turningOn,
  resetting,
  unsupported,
  unauthorized,
  unknown
}

enum BleWriteCharacteristicType { writeWithoutResponse, writeWithResponse }

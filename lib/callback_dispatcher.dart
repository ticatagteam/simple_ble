import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


void callbackDispatcher() {
  const MethodChannel backgroundChannel =
  MethodChannel('plugins.flutter.io/simpleble_plugin_background');
  WidgetsFlutterBinding.ensureInitialized();

  backgroundChannel.setMethodCallHandler((MethodCall call) async {
    final List<dynamic> args = call.arguments;
    final Function callback = PluginUtilities.getCallbackFromHandle(
        CallbackHandle.fromRawHandle(args[0]));
    assert(callback != null);
    callback();
  });
  backgroundChannel.invokeMethod('initialized');
}
package com.fvisticot.simple_ble;

import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static android.bluetooth.BluetoothDevice.TRANSPORT_LE;

public class PendingIntentScanReceiver extends BroadcastReceiver {

    final String batteryServiceId = "0000180f-0000-1000-8000-00805f9b34fb";
    final String ticatagServiceId = "0000ffe0-0000-1000-8000-00805f9b34fb";
    final String alertServiceId = "00001802-0000-1000-8000-00805f9b34fb";

    final String batteryLevelCharId = "00002a19-0000-1000-8000-00805f9b34fb";
    final String buttonPressedCharId = "0000ffe1-0000-1000-8000-00805f9b34fb";
    final String alertCharId = "00002a06-0000-1000-8000-00805f9b34fb";

    private final BluetoothGattCallback _gattCallback =
            new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                                    int newState) {
                    Log.i("fs", "==>>>New state: " + Integer.toString(newState));
                    switch (newState) {
                        case BluetoothProfile
                                .STATE_CONNECTED:
                            Log.i("state", "===>CONNECTED");
                            break;
                        case BluetoothProfile.STATE_DISCONNECTED:
                            Log.i("state", "===DISCONNECTED");
                    }
                }
            };

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra(BluetoothLeScanner.EXTRA_LIST_SCAN_RESULT)) {
            ArrayList<ScanResult> results = intent.getParcelableArrayListExtra(BluetoothLeScanner.EXTRA_LIST_SCAN_RESULT);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 42, intent, FLAG_UPDATE_CURRENT);
            BluetoothManager bluetoothManager =
                    (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
            BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
            bluetoothAdapter.getBluetoothLeScanner().stopScan(pendingIntent);
            for (final ScanResult result : results) {
                Log.e("onScanResult", "====>onScanResult : " + result.getDevice().getName() + " " + result.getDevice().getAddress());
                this.connect(context, result.getDevice().getAddress());
            }
        }
    }

    public void connect(final Context context, String address) {
        Log.i("fqs", "Connect to " + address);
        BluetoothManager bluetoothManager =
                (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        final BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        final BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
        Log.i("dev", "Device: " + device.getAddress());
        if (device != null) {
            device.connectGatt(context, false, new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                                    int newState) {
                    Log.i("fs", "==>>>New state: " + Integer.toString(newState));
                    switch (newState) {
                        case BluetoothProfile
                                .STATE_CONNECTED:
                            Log.i("state", "===>CONNECTED");
                            gatt.discoverServices();
                            break;
                        case BluetoothProfile.STATE_DISCONNECTED:
                            Log.i("state", "===DISCONNECTED");
                            BleDeviceScanner.getInstance().startScan(context, bluetoothAdapter, true, new BleDeviceScanner.OnDeviceScannedListener() {
                                @Override
                                public void onDeviceScanned(ScanResult result) {
                                    ;
                                }
                            });
                    }
                }

                @Override
                public void onCharacteristicRead(BluetoothGatt gatt,
                                                 BluetoothGattCharacteristic characteristic,
                                                 int status) {
                    int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                    Log.i("battery", Integer.toString(value));

                }

                @Override
                public void onCharacteristicChanged (BluetoothGatt gatt,
                                                     BluetoothGattCharacteristic characteristic) {
                  Log.i("aaa", "onCharacteristicChanged");
                    int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                    Log.i("button", Integer.toString(value));
                }

                @Override
                public void onServicesDiscovered(BluetoothGatt gatt, int status) {

                    BluetoothGattService alertService = gatt.getService(UUID.fromString(alertServiceId));
                    if (alertService != null) {
                        BluetoothGattCharacteristic characteristic = alertService.getCharacteristic(UUID.fromString(alertCharId));
                        if (characteristic != null) {
                            characteristic.setValue(new byte[]{1});
                            gatt.writeCharacteristic(characteristic);
                        }
                    }
/*
                    BluetoothGattService batteryService = gatt.getService(UUID.fromString(batteryServiceId));
                    if (batteryService != null) {
                        BluetoothGattCharacteristic characteristic = batteryService.getCharacteristic(UUID.fromString(batteryLevelCharId));
                        if (characteristic != null) {
                            Log.i("fqsd", "readChar");
                            //gatt.readCharacteristic(characteristic);
                        }
                    }


                    BluetoothGattService ticatagService = gatt.getService(UUID.fromString(ticatagServiceId));
                    if (ticatagService != null) {
                        BluetoothGattCharacteristic characteristic = ticatagService.getCharacteristic(UUID.fromString(buttonPressedCharId));
                        if (characteristic != null) {
                            Log.i("fqsd", "readChar");
                            gatt.setCharacteristicNotification(characteristic, true);
                            List<BluetoothGattDescriptor> descriptors =  characteristic.getDescriptors();
                            for (BluetoothGattDescriptor descriptor : descriptors) {
                                Log.i("fqsfd", descriptor.getUuid().toString());
                            }
                            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
                            if (descriptor != null) {
                                Log.i("fqsdf", "DESCRIPTOR OK");
                                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                                boolean connected = gatt.writeDescriptor(descriptor);
                            }
                        }
                    }*/


                }
            }, TRANSPORT_LE);
        }
    }
}

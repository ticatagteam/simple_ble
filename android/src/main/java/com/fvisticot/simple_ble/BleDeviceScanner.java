package com.fvisticot.simple_ble;

import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static android.bluetooth.BluetoothDevice.TRANSPORT_LE;

public class BleDeviceScanner {

    final String batteryServiceId = "0000180f-0000-1000-8000-00805f9b34fb";
    final String ticatagServiceId = "0000ffe0-0000-1000-8000-00805f9b34fb";
    final String alertServiceId = "00001802-0000-1000-8000-00805f9b34fb";

    final String batteryLevelCharId = "00002a19-0000-1000-8000-00805f9b34fb";
    final String buttonPressedCharId = "0000ffe1-0000-1000-8000-00805f9b34fb";
    final String alertCharId = "00002a06-0000-1000-8000-00805f9b34fb";


    private static BleDeviceScanner _instance = null;

    private BleDeviceScanner() {

    }

    public static BleDeviceScanner getInstance() {
        if (_instance == null) {
            _instance = new BleDeviceScanner();
        }
        return _instance;
    }

    public void startScan(Context context, BluetoothAdapter bluetoothAdapter, boolean isPendingIntent, final OnDeviceScannedListener listener) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Log.i("aa", "Starting scan");
            ScanSettings settings = (new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .setPhy(ScanSettings.PHY_LE_ALL_SUPPORTED)).build();


            List<ScanFilter> listOfFiters = new ArrayList<>();
            ScanFilter.Builder builder = new ScanFilter.Builder();
            builder.setDeviceName("ticatag");
            //builder.setDeviceAddress("FA:99:B1:8C:12:2E");
            //builder.setServiceUuid(new ParcelUuid(this.convertFromInteger(2013)));
            ScanFilter filter = builder.build();
            listOfFiters.add(filter);



            if (isPendingIntent) {
                Intent intent = new Intent(context, PendingIntentScanReceiver.class);
                intent.setAction("com.example.bletest.ACTION_FOUND");
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 42, intent, FLAG_UPDATE_CURRENT);
                int scanRes = bluetoothAdapter.getBluetoothLeScanner().startScan(listOfFiters, settings, pendingIntent);
            } else {
                bluetoothAdapter.getBluetoothLeScanner().startScan(listOfFiters, settings, new ScanCallback() {
                    @Override
                    public void onScanResult(int callbackType, ScanResult result) {
                        super.onScanResult(callbackType, result);
                        //Log.e("SERVICEDATA", result.get)
                        //Log.e("onScanResult", "====>onScanResult : RSSI: " + result.getRssi() + " Name: " + result.getDevice().getName() + " " + result.getDevice().getAddress());
                        listener.onDeviceScanned(result);
                    }
                });
            }
        }
    }

    public void connect(final Context context, final BluetoothAdapter bluetoothAdapter, String address) {
        Log.i("fd", "Connect to " + address);
        final BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
        Log.i("dev", "Device: " + device.getAddress());
        if (device != null) {
            device.connectGatt(context, false, new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                                    int newState) {
                    Log.i("fs", "==>>>New state: " + Integer.toString(newState));
                    switch (newState) {
                        case BluetoothProfile
                                .STATE_CONNECTED:
                            Log.i("state", "===>CONNECTED");
                            gatt.discoverServices();
                            break;
                        case BluetoothProfile.STATE_DISCONNECTED:
                            Log.i("state", "===DISCONNECTED");
                            BleDeviceScanner.getInstance().startScan(context, bluetoothAdapter, true, new BleDeviceScanner.OnDeviceScannedListener() {
                                @Override
                                public void onDeviceScanned(ScanResult result) {
                                    ;
                                }
                            });
                    }
                }

                @Override
                public void onCharacteristicRead(BluetoothGatt gatt,
                                                 BluetoothGattCharacteristic characteristic,
                                                 int status) {
                    int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                    Log.i("battery", Integer.toString(value));

                }

                @Override
                public void onCharacteristicChanged (BluetoothGatt gatt,
                                                     BluetoothGattCharacteristic characteristic) {
                    Log.i("aaa", "onCharacteristicChanged");
                    int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                    Log.i("button", Integer.toString(value));
                }

                @Override
                public void onServicesDiscovered(BluetoothGatt gatt, int status) {

                    BluetoothGattService alertService = gatt.getService(UUID.fromString(alertServiceId));
                    if (alertService != null) {
                        BluetoothGattCharacteristic characteristic = alertService.getCharacteristic(UUID.fromString(alertCharId));
                        if (characteristic != null) {
                            characteristic.setValue(new byte[]{1});
                            gatt.writeCharacteristic(characteristic);
                        }
                    }
                }
            }, TRANSPORT_LE);
        }
    }

    private UUID convertFromInteger(int i) {
        final long MSB = 0x0000000000001000L;
        final long LSB = 0x800000805f9b34fbL;
        long value = i & 0xFFFFFFFF;
        return new UUID(MSB | (value << 32), LSB);
    }

    public interface OnDeviceScannedListener{
        public void onDeviceScanned(ScanResult result);   //method, which can have parameters
    }
}



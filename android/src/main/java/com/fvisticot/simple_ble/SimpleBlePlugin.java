package com.fvisticot.simple_ble;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;


import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import static android.bluetooth.BluetoothDevice.DEVICE_TYPE_UNKNOWN;
import static android.bluetooth.BluetoothDevice.TRANSPORT_LE;
import static io.flutter.plugin.common.PluginRegistry.*;

/**
 * SimpleBlePlugin
 */
public class SimpleBlePlugin implements MethodCallHandler, ActivityResultListener {

    private static final int REQUEST_COARSE_LOCATION_PERMISSIONS = 1452;
    private static final String TAG = "SimpleBlePlugin";

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothManager mBluetoothManager;
    private Activity activity;
    private MethodCall pendingCall;
    private Result pendingResult;
    private Map<String, BluetoothDeviceCache> mDevices = new HashMap<String, BluetoothDeviceCache>();

    private RequestPermissionsResultListener mPermissionsResultListener;
    private boolean waitingForPermission = false;
    private int locationPermissionState;
    private Result result;
    private static EventChannel.EventSink detectedDevicesSink;
    private static EventChannel.EventSink devicesStateSink;
    private static EventChannel.EventSink characteristicNotificationsSink;

    private Result discoverServicesResult;
    private Result readCharactericResult;
    private Result rssiResult;

    private EventChannel.StreamHandler characteristicNotificationsStateStreamHandler = new EventChannel.StreamHandler() {
        @Override
        public void onListen(Object o, EventChannel.EventSink eventSink) {
            characteristicNotificationsSink = eventSink;
        }

        @Override
        public void onCancel(Object o) {
            ;
        }
    };

    private EventChannel.StreamHandler devicesStateStreamHandler = new EventChannel.StreamHandler() {
        @Override
        public void onListen(Object o, EventChannel.EventSink eventSink) {
            devicesStateSink = eventSink;
        }

        @Override
        public void onCancel(Object o) {
            ;
        }
    };

    private EventChannel.StreamHandler detectedDevicesStreamHandler = new EventChannel.StreamHandler() {
        @Override
        public void onListen(Object o, EventChannel.EventSink eventSink) {
            detectedDevicesSink = eventSink;
        }

        @Override
        public void onCancel(Object o) {
            ;
        }
    };

    private EventChannel.StreamHandler stateStreamHandler = new EventChannel.StreamHandler() {
        private EventChannel.EventSink stateSink;

        private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final String action = intent.getAction();

                if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                    final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                            BluetoothAdapter.ERROR);
                    final int apiState;
                    switch (state) {
                        case BluetoothAdapter.STATE_OFF:
                            apiState = 4;
                            break;
                        case BluetoothAdapter.STATE_ON:
                            apiState = 5;
                            break;
                        case BluetoothAdapter.STATE_TURNING_OFF:
                            apiState = 6;
                            break;
                        case BluetoothAdapter.STATE_TURNING_ON:
                            apiState = 7;
                            break;
                        default:
                            apiState = 0;
                            break;
                    }
                    activity.runOnUiThread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    stateSink.success(apiState);
                                }
                            });

                }
            }
        };

        @Override
        public void onListen(Object o, EventChannel.EventSink eventSink) {
            stateSink = eventSink;
            IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            activity.registerReceiver(mReceiver, filter);
        }

        @Override
        public void onCancel(Object o) {
            stateSink = null;
            activity.unregisterReceiver(mReceiver);
        }
    };


    SimpleBlePlugin(Activity activity) {
        if (activity != null) {
            this.activity = activity;
            this.mBluetoothManager = (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);
            this.mBluetoothAdapter = mBluetoothManager.getAdapter();
        }
    }

    public static void registerWith(Registrar registrar) {
        if (registrar != null) {
            final MethodChannel channel = new MethodChannel(registrar.messenger(), "simple_ble");
            final SimpleBlePlugin simpleBlePlugin = new SimpleBlePlugin(registrar.activity());
            channel.setMethodCallHandler(simpleBlePlugin);

            final EventChannel detectedDevicesEventChannel = new EventChannel(registrar.messenger(), "detected-devices");
            detectedDevicesEventChannel.setStreamHandler(simpleBlePlugin.detectedDevicesStreamHandler);

            final EventChannel stateEventChannel = new EventChannel(registrar.messenger(), "state");
            stateEventChannel.setStreamHandler(simpleBlePlugin.stateStreamHandler);

            final EventChannel devicesStateEventChannel = new EventChannel(registrar.messenger(), "devices-state");
            devicesStateEventChannel.setStreamHandler(simpleBlePlugin.devicesStateStreamHandler);

            final EventChannel characteristicNotificationsEventChannel = new EventChannel(registrar.messenger(), "notifications");
            characteristicNotificationsEventChannel.setStreamHandler(simpleBlePlugin.characteristicNotificationsStateStreamHandler);

            registrar.addRequestPermissionsResultListener(simpleBlePlugin.getPermissionsResultListener());
        }
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        Log.i(TAG, "Method call: " + call.method);
        switch (call.method) {
            case "readRssi": {
                String deviceId = call.arguments();
                Log.i(TAG, "readRssi for identifier: " + deviceId);
                try {
                    BluetoothGatt gatt = locateGatt(deviceId);
                    if (gatt != null) {
                        rssiResult = result;
                        boolean res = gatt.readRemoteRssi();
                        if (!res) {
                            result.error("read_rssi_error", "Can not read RSSI", null);
                        }
                    } else {
                        result.error("peripheral_not_found", "Peripheral not found", null);
                    }
                } catch (Exception e) {
                    result.error("discover_services_error", e.getMessage(), e);
                }
                break;
            }
            case "enableBle":
                if (mBluetoothAdapter != null) {
                    if (!mBluetoothAdapter.isEnabled()) {
                        boolean res = mBluetoothAdapter.enable();
                        result.success(res);
                    } else {
                        result.success(true);
                    }
                } else {
                    result.error("BluetoothAdapter is null", null, null);
                }

                break;
            case "disableBle":
                if (mBluetoothAdapter != null) {
                    if (mBluetoothAdapter.isEnabled()) {
                        boolean res = mBluetoothAdapter.disable();
                        result.success(res);
                    } else {
                        result.error("Bluetooth is currently not enabled", null, null);
                    }
                } else {
                    result.error("BluetoothAdapter is null", null, null);
                }

                break;
            case "isAvailable":
                result.success(mBluetoothAdapter != null);
                break;
            case "bleState":
                int state;
                switch (mBluetoothAdapter.getState()) {
                    case BluetoothAdapter.STATE_OFF:
                        state = 4;
                        break;
                    case BluetoothAdapter.STATE_ON:
                        state = 5;
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        state = 6;
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        state = 7;
                        break;
                    default:
                        state = 0;
                        break;
                }
                result.success(state);
                break;
            case "startScan":
                this.result = result;
                this.pendingCall = call;
                this.pendingResult = result;
                if (!checkPermissions()) {
                    requestPermissions();
                } else {
                    startScan(call, result);
                    result.success(1);
                }
                break;
            case "devicesWithIdentifiers": {
                Map<String, Object> map = call.arguments();
                Log.i(TAG, map.toString());
                List<String> identifiers = (List<String>) map.get("identifiers");
                Log.i(TAG, identifiers.toString());
                List<Map<String, Object>> res = new ArrayList<>();
                for (final String identifier : identifiers) {
                    Map<String, Object> deviceMap = new HashMap<>();
                    deviceMap.put("identifier", identifier);
                    deviceMap.put("name", "ticatag");
                    deviceMap.put("state", 2);
                    res.add(deviceMap);
                }
                Log.i(TAG, Integer.toString(res.size()));
                result.success(res);
                break;
            }
            case "connectedDevices":
                if (mBluetoothManager == null) {
                    result.error("500", "BluetoothManager is null", null);
                }
                List<BluetoothDevice> connectedDevices = mBluetoothManager.getConnectedDevices(BluetoothGatt.GATT);
                Log.i(TAG, "TEST2" + connectedDevices);
                List<Map<String, Object>> res = new ArrayList<>();
                for (final BluetoothDevice device : connectedDevices) {
                    Map<String, Object> deviceMap = new HashMap<>();
                    deviceMap.put("identifier", device.getAddress());
                    deviceMap.put("name", device.getName());
                    deviceMap.put("state", 2);
                    res.add(deviceMap);
                }
                result.success(res);
                break;
            case "connectDevice": {
                String deviceId = call.arguments();
                connect(null, mBluetoothAdapter, deviceId);
                result.success(null);
                break;
            }
            case "cancelDeviceConnection": {
                String deviceId = call.arguments();

                disconnect(null, mBluetoothAdapter, deviceId);
                result.success(null);
                break;
            }
            case "discoverServices": {
                final Map<String, Object> map = call.arguments();
                String deviceId = (String) map.get("identifier");
                try {
                    BluetoothGatt gatt = locateGatt(deviceId);
                    if (gatt.discoverServices()) {
                        discoverServicesResult = result;
                        //result.success(null);
                    } else {
                        result.error("discover_services_error", "unknown reason", null);
                    }
                } catch (Exception e) {
                    result.error("discover_services_error", e.getMessage(), e);
                }
                break;
            }
            case "discoverCharacteristics": {
                final Map<String, Object> map = call.arguments();
                String deviceId = (String) map.get("identifier");
                final Map<String, Object> serviceMap = call.argument("service");
                final String serviceId = (String) serviceMap.get("uuid");
                try {
                    BluetoothGatt gatt = locateGatt(deviceId);
                    BluetoothGattService service = gatt.getService(UUID.fromString(serviceId));
                    if (service != null) {
                        List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                        List<Map<String, Object>> resIt = new ArrayList();
                        for (final BluetoothGattCharacteristic characteristic : characteristics) {
                            Map<String, Object> mapIt = new HashMap<String, Object>();
                            Map<String, Object> serviceMapIt = new HashMap<String, Object>();
                            Map<String, Object> characteristicMapIt = new HashMap<String, Object>();
                            characteristicMapIt.put("uuid", characteristic.getUuid().toString());
                            serviceMapIt.put("uuid", serviceId);
                            mapIt.put("service", serviceMapIt);
                            mapIt.put("characteristic", characteristicMapIt);
                            resIt.add(mapIt);
                        }
                        result.success(resIt);
                    }
                    return;
                } catch (Exception e) {
                    result.error("discover_characteristics_error", e.getMessage(), e);
                }
            }
            case "setNotifyValueForCharacteristic": {
                final String deviceId = call.argument("identifier");
                final Map<String, Object> map = call.argument("characteristic");
                final String characteristicUuid = (String) map.get("uuid");
                final String serviceUuid = (String) map.get("serviceUuid");

                try {
                    BluetoothGatt gatt = locateGatt(deviceId);
                    BluetoothGattService service = gatt.getService(UUID.fromString(serviceUuid));
                    if (service != null) {
                        BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicUuid));
                        if (characteristic != null) {
                            gatt.setCharacteristicNotification(characteristic, true);
                            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
                            if (descriptor != null) {
                                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                                boolean connected = gatt.writeDescriptor(descriptor);
                                result.success(null);
                            } else {
                                result.error("discover_characteristics_error", "descriptor not found", null);
                            }
                        } else {
                            result.error("discover_characteristics_error", "characteristic not found", null);
                        }
                    } else {
                        result.error("discover_characteristics_error", "service not found", null);
                    }
                } catch (Exception e) {
                    result.error("discover_characteristics_error", e.getMessage(), e);
                }
                break;
            }
            case "readValueForCharacteristic": {
                final String deviceId = call.argument("identifier");
                final Map<String, Object> map = call.argument("characteristic");
                final String characteristicUuid = (String) map.get("uuid");
                final String serviceUuid = (String) map.get("serviceUuid");
                Log.i(TAG, "Read value for char: " + characteristicUuid);
                try {
                    BluetoothGatt gatt = locateGatt(deviceId);
                    BluetoothGattService service = gatt.getService(UUID.fromString(serviceUuid));
                    if (service != null) {
                        BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicUuid));
                        if (characteristic != null) {
                            readCharactericResult = result;
                            gatt.readCharacteristic(characteristic);
                            return;
                        } else {
                            result.error("discover_characteristics_error", "characteristic not found", null);
                        }
                    } else {
                        result.error("discover_characteristics_error", "service not found", null);
                    }
                } catch (Exception e) {
                    result.error("discover_characteristics_error", e.getMessage(), e);
                }
            }
            case "writeValueForCharacteristic": {
                final String deviceId = call.argument("identifier");
                //final char value = call.argument("value");
                final Map<String, Object> map = call.argument("characteristic");
                final String characteristicUuid = (String) map.get("uuid");
                final String serviceUuid = (String) map.get("serviceUuid");
                try {
                    BluetoothGatt gatt = locateGatt(deviceId);
                    BluetoothGattService service = gatt.getService(UUID.fromString(serviceUuid));
                    if (service != null) {
                        BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicUuid));
                        if (characteristic != null) {
                            characteristic.setValue(new byte[]{1});
                            gatt.writeCharacteristic(characteristic);
                            return;
                        } else {
                            result.error("discover_characteristics_error", "characteristic not found", null);
                        }
                    } else {
                        result.error("discover_characteristics_error", "service not found", null);
                    }

                    return;
                } catch (Exception e) {
                    result.error("discover_characteristics_error", e.getMessage(), e);
                }
            }
            default:
                result.notImplemented();
        }
    }


    private BluetoothGatt locateGatt(String remoteId) throws Exception {
        BluetoothDeviceCache cache = mDevices.get(remoteId);
        if (cache == null || cache.gatt == null) {
            throw new Exception("no instance of BluetoothGatt, have you connected first?");
        } else {
            return cache.gatt;
        }
    }

    private void startScan(MethodCall call, Result result) {
        BleDeviceScanner.getInstance().startScan(null, mBluetoothAdapter, false, new BleDeviceScanner.OnDeviceScannedListener() {
            @Override
            public void onDeviceScanned(ScanResult result) {
                Map<String, Object> map = new HashMap<>();
                Map<String, Object> deviceMap = new HashMap<>();
                map.put("rssi", result.getRssi());
                deviceMap.put("identifier", result.getDevice().getAddress());
                deviceMap.put("name", result.getDevice().getName());
                deviceMap.put("state", 3);
                map.put("device", deviceMap);
                //Map<ParcelUuid, byte[]> serviceDataMap=result.getScanRecord().getServiceData();
                //map.put("serviceData", serviceDataMap);
                if (detectedDevicesSink != null) {
                    detectedDevicesSink.success(map);
                }
            }
        });
    }


    private BluetoothGatt connectGattCompat(final Context context, BluetoothGattCallback bluetoothGattCallback, BluetoothDevice device, boolean autoConnect) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return device.connectGatt(context, autoConnect, bluetoothGattCallback, TRANSPORT_LE);
        } else {
            return device.connectGatt(context, autoConnect, bluetoothGattCallback);
        }
    }

    public void disconnect(final Context context, final BluetoothAdapter bluetoothAdapter, String deviceId) {
        Log.i(TAG, "Disconnnect for device: " + deviceId);
        try {
            BluetoothGatt gatt = locateGatt(deviceId);
            gatt.disconnect();
            //gatt.close();
        } catch (Exception e) {

        }

    }

    public void connect(final Context context, final BluetoothAdapter bluetoothAdapter, String deviceId) {
        Log.i(TAG, "Connect to " + deviceId);
        final BluetoothDevice device = bluetoothAdapter.getRemoteDevice(deviceId);
        if (device != null) {
            final int deviceType = device.getType();
            Log.i(TAG, "DeviceType " + deviceType);
            if (deviceType == DEVICE_TYPE_UNKNOWN) {
                Log.e(TAG, "DeviceType is unknown, need to scan again");
            }

            connectGattCompat(context, new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, final int status,
                                                    int newState) {
                    switch (newState) {
                        case BluetoothProfile
                                .STATE_CONNECTED:
                            mDevices.put(device.getAddress(), new BluetoothDeviceCache(gatt));
                            if (devicesStateSink != null) {
                                activity.runOnUiThread(
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                Map<String, Object> map = new HashMap<String, Object>();
                                                map.put("state", new Integer(2));
                                                map.put("cause", new Integer(0));
                                                map.put("identifier", device.getAddress());
                                                devicesStateSink.success(map);
                                            }
                                        });

                            }
                            break;
                        case BluetoothProfile.STATE_DISCONNECTED:
                            gatt.close();
                            if (devicesStateSink != null) {
                                activity.runOnUiThread(
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                Map<String, Object> map = new HashMap<String, Object>();
                                                map.put("state", new Integer(0));
                                                map.put("cause", new Integer(status));
                                                map.put("identifier", device.getAddress());
                                                devicesStateSink.success(map);
                                                //devicesStateSink.success({state: 0, cause: status});
                                            }
                                        });
                            }
                            break;
                        default: {
                            Log.e(TAG, "State not managed " + newState);
                        }
                    }

                }

                @Override
                public void onCharacteristicRead(BluetoothGatt gatt,
                                                 final BluetoothGattCharacteristic characteristic,
                                                 int status) {
                    final byte[] value = characteristic.getValue();
                    activity.runOnUiThread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    Map<String, Object> map = new HashMap<String, Object>();
                                    map.put("value", value);
                                    Map<String, Object> characteristicMap = new HashMap<String, Object>();
                                    characteristicMap.put("uuid", characteristic.getUuid().toString());
                                    map.put("characteristic", characteristicMap);
                                    readCharactericResult.success(map);
                                }
                            });

                }

                @Override
                public void onCharacteristicChanged(BluetoothGatt gatt,
                                                    final BluetoothGattCharacteristic characteristic) {
                    Log.i(TAG, "onCharacteristicChanged");
                    //final int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                    final byte[] value = characteristic.getValue();
                    //Log.i("button", Integer.toString(value));
                    activity.runOnUiThread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    Map<String, Object> map = new HashMap<String, Object>();
                                    map.put("value", value);
                                    Map<String, Object> characteristicMap = new HashMap<String, Object>();
                                    characteristicMap.put("uuid", characteristic.getUuid().toString());
                                    map.put("characteristic", characteristicMap);
                                    if (characteristicNotificationsSink != null) {
                                        characteristicNotificationsSink.success(map);
                                    }

                                }
                            });
                }

                @Override
                public void onReadRemoteRssi(BluetoothGatt gatt, final int rssi, int status) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        Log.i(TAG, "onReadRemoteRssi: " + rssi);
                        activity.runOnUiThread(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        rssiResult.success(rssi);
                                    }
                                });
                    }
                }

                @Override
                public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                    Log.i(TAG, "onServicesDiscovered with status" + status);
                    List<BluetoothGattService> services = gatt.getServices();
                    final List<Map> res = new ArrayList<Map>();
                    for (final BluetoothGattService service : services) {
                        Map<String, Object> map = new HashMap<String, Object>();
                        map.put("uuid", service.getUuid().toString());
                        res.add(map);
                    }
                    activity.runOnUiThread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    discoverServicesResult.success(res);
                                }
                            });
                }
            }, device, true);

            /*BluetoothGatt gattServer = device.connectGatt(context, true, new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                                    int newState) {
                    switch (newState) {
                        case BluetoothProfile
                                .STATE_CONNECTED:
                            Log.i(TAG, "===>CONNECTED");
                            mDevices.put(device.getAddress(), new BluetoothDeviceCache(gatt));
                            if (devicesStateSink != null) {

                                activity.runOnUiThread(
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                devicesStateSink.success(2);
                                            }
                                        });

                            }
                            break;
                        case BluetoothProfile.STATE_DISCONNECTED:
                            Log.i(TAG, "===DISCONNECTED with Status: " + Integer.toString(status));
                            if (devicesStateSink != null) {
                                activity.runOnUiThread(
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                devicesStateSink.success(0);
                                            }
                                        });
                            }
                    }
                }

                @Override
                public void onCharacteristicRead(BluetoothGatt gatt,
                                                 final BluetoothGattCharacteristic characteristic,
                                                 int status) {
                    //final int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                    final byte[] value = characteristic.getValue();
                    activity.runOnUiThread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    Map<String, Object> map = new HashMap<String, Object>();
                                    map.put("value", value);
                                    Map<String, Object> characteristicMap = new HashMap<String, Object>();
                                    characteristicMap.put("uuid", characteristic.getUuid().toString());
                                    map.put("characteristic", characteristicMap);
                                    readCharactericResult.success(map);
                                }
                            });

                }

                @Override
                public void onCharacteristicChanged (BluetoothGatt gatt,
                                                     final BluetoothGattCharacteristic characteristic) {
                    Log.i(TAG, "onCharacteristicChanged");
                    //final int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                    final byte[] value = characteristic.getValue();
                    //Log.i("button", Integer.toString(value));
                    activity.runOnUiThread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    Map<String, Object> map = new HashMap<String, Object>();
                                    map.put("value", value);
                                    Map<String, Object> characteristicMap = new HashMap<String, Object>();
                                    characteristicMap.put("uuid", characteristic.getUuid().toString());
                                    map.put("characteristic", characteristicMap);
                                    if (characteristicNotificationsSink != null) {
                                        characteristicNotificationsSink.success(map);
                                    }

                                }
                            });
                }

                @Override
                public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                    Log.i(TAG, "onServicesDiscovered with status" + status);
                    List<BluetoothGattService> services=gatt.getServices();
                    final List<Map> res = new ArrayList<Map>();
                    for (final BluetoothGattService service : services) {
                        Map<String, Object> map = new HashMap<String, Object>();
                        map.put("uuid", service.getUuid().toString());
                        res.add(map);
                    }
                    activity.runOnUiThread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    discoverServicesResult.success(res);
                                }
                            });
                }
            }, TRANSPORT_LE);*/

        }
    }

    public RequestPermissionsResultListener getPermissionsResultListener() {
        return mPermissionsResultListener;
    }

    private void createPermissionsResultListener() {
        mPermissionsResultListener = new RequestPermissionsResultListener() {
            @Override
            public boolean onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
                if (requestCode == REQUEST_COARSE_LOCATION_PERMISSIONS && permissions.length == 1 && permissions[0].equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    if (waitingForPermission) {
                        waitingForPermission = false;
                        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                            result.success(1);
                        } else {
                            result.success(0);
                        }
                        result = null;
                    }
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (result != null) {
                            startScan(pendingCall, pendingResult);
                        }
                    } else {
                        if (!shouldShowRequestPermissionRationale()) {
                            if (result != null) {
                                result.error("PERMISSION_DENIED_NEVER_ASK", "Location permission denied forever- please open app settings", null);
                            }
                        } else {
                            if (result != null) {
                                result.error("PERMISSION_DENIED", "Location permission denied", null);
                            }
                        }
                    }
                    return true;
                }
                return false;
            }
        };
    }

    private boolean checkPermissions() {
        this.locationPermissionState = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
        return this.locationPermissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_COARSE_LOCATION_PERMISSIONS);
    }

    private boolean shouldShowRequestPermissionRationale() {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    this.result.success(1);
                } else {
                    this.result.success(0);
                }
                break;
            default:
                return false;
        }
        return true;
    }


    class BluetoothDeviceCache {
        final BluetoothGatt gatt;

        BluetoothDeviceCache(BluetoothGatt gatt) {
            this.gatt = gatt;
        }
    }

}
